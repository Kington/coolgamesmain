﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace CoolGames.Models
{
    public class BDPanierRepository : IPanierRepository
    {
        private readonly GamesDbContext _dbContext;
        public string? PanierSessionId;
        /// <summary>
        /// Récupère la liste des articles présents dans le panier correspondant à l'identifiant de session du panier
        /// Les jeux associés à chaque article sont inclus
        /// </summary>
        public List<Panier> ArticlePanier
        {
            get
            {
                return _dbContext.Paniers.Where(p => p.PanierSessionId== PanierSessionId).Include(p => p.Jeu).ToList();
            }
        }

        public BDPanierRepository(GamesDbContext context)
        {
            _dbContext = context;
            

        }
        /// <summary>
        /// Ajoute un jeu au panier en fonction de son identifiant. Si le jeu est déjà présent dans le panier, sa quantité est augmentée de 1
        /// Sinon, un nouvel article est créé dans le panier avec une quantité de et on l'enregistre dans la base de donnée
        /// 
        /// </summary>
        /// <param name="jeu"></param>
        public void AjouterAuPanier(Jeu jeu)
        {
            var panier = _dbContext.Paniers.FirstOrDefault(p => p.PanierSessionId == PanierSessionId && p.JeuId == jeu.Id);

            if (panier != null)
            {
                panier.Quantite++;
            }
            else
            {
                panier = new Panier
                {
                    Id = 0,
                    JeuId = jeu.Id,
                    Jeu = jeu,
                    Quantite = 1,
                    PanierSessionId = PanierSessionId
                };
                _dbContext.Paniers.Add(panier);
            }
            _dbContext.SaveChanges();
        }
        /// <summary>
        /// Calcule le montant total du panier en multipliant les prix des jeux avec leur quantité
        /// </summary>
        /// <returns></returns>
        public decimal MontantTotalPanier()
        {
            var montantTotal = _dbContext.Paniers
                .Where(p => p.PanierSessionId == PanierSessionId)
                .Select(p => p.Quantite * p.Jeu.Prix)
                .Sum();
            return (decimal)montantTotal;
        }
        /// <summary>
        /// Calcule le nombre total d'articles présents dans le panier en additionnant les quantités de chaque article
        /// </summary>
        /// <param name="PanierSessionId"></param>
        /// <returns></returns>
        public int NombreArticlesPanier(string PanierSessionId)
        {
            var totalquantitepanier = _dbContext.Paniers.Where(p => p.PanierSessionId == (PanierSessionId)).Sum(panier => panier.Quantite);

            return totalquantitepanier;
        }
        /// <summary>
        /// Définit l'identifiant de session du panier
        /// </summary>
        /// <param name="sessionId"></param>
        public void SetSessionId(string sessionId)
        {
            PanierSessionId = sessionId;
        }
        /// <summary>
        /// Supprime un jeu spécifique du panier en fonction de son identifiant
        /// Si la quantité de jeu dans le panier est supérieure à 1, la quantité est réduite de 1.
        /// Sinon, l'article est entièrement supprimé du panier et on l'enregistre dans la base de donnée
        /// </summary>
        /// <param name="jeu"></param>
        public void SupprimerDuPanier(Jeu jeu)
        {
            var panier = _dbContext.Paniers.FirstOrDefault(p => p.JeuId == jeu.Id && p.PanierSessionId == PanierSessionId);

            if (panier != null)
            {
                if (panier.Quantite > 1)
                {
                    panier.Quantite--;
                }
                else
                {
                    _dbContext.Paniers.Remove(panier);
                    
                }
                _dbContext.SaveChanges();
            }          
        }
        /// <summary>
        /// Supprimer tous les jeux dans le panier et l'enregistre dans la base de donnée
        /// </summary>
        public void ViderPanier()
        {
            var panier = _dbContext.Paniers.Where(p => p.PanierSessionId == (PanierSessionId)).ToList();

            if (panier != null && panier.Count > 0)
            {
                _dbContext.Paniers.RemoveRange(panier);
                _dbContext.SaveChanges();
            }
        }
        /// <summary>
        ///   Initialise l'identifiant de session du panier
        /// </summary>
        /// <param name="sessionId"></param>
        public void InitialiserPanierId(string sessionId)
        {
            PanierSessionId = sessionId;
        }

     
    }
}
