﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CoolGames.Migrations
{
    /// <inheritdoc />
    public partial class deleteColonnePanier : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Paniers");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Paniers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
