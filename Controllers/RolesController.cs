﻿using CoolGames.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CoolGames.Controllers
{
    /// <summary>
    /// Méthodes générales qui permet de créer un role et d'assigner un utilisateur au role
    /// </summary>
    public class RolesController : Controller
    {
        private RoleManager<IdentityRole> _roleManager;
        private UserManager<IdentityUser> _userManager;

        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<IdentityUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        [Authorize(Roles = "Admin")]
        public ViewResult Index()
        {
            return View(_roleManager.Roles);
        }
        [Authorize(Roles = "Admin")]
        public IActionResult Creer()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Creer(string nom)
        {
            IdentityRole role = await _roleManager.FindByNameAsync(nom);

            if (role != null)
            {
                ModelState.AddModelError(nom, "Ce nom de rôle existe déjà, veuillez choisir un autre.");
                return View(nameof(Creer), nom);
            }

            if (ModelState.IsValid)
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(nom));

                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                {
                    foreach (IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }

            return View(nameof(Creer), nom);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Supprimer(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);

            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);

                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                {
                    foreach (IdentityError error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }

            return View("Index", _roleManager.Roles);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> ModifierRoleUsager(string roleId)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(roleId);

            IList<IdentityUser> membres = new List<IdentityUser>();
            List<IdentityUser> nonMembres = new List<IdentityUser>();

            List<IdentityUser> listeUsagers = _userManager.Users.ToList();

            membres = await _userManager.GetUsersInRoleAsync(role.Name);
            nonMembres = _userManager.Users.Where(user => !membres.Contains(user)).ToList();

            RoleMembresViewModel membresRoleVM = new RoleMembresViewModel
            {
                Role = role.Name,
                RoleId = role.Id,
                NonMembres = nonMembres,
                Membres = membres
            };

            return View(membresRoleVM);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> ModifierRoleUsager(RoleMembresViewModel model)
        {
            IdentityResult results;
            if (!ModelState.IsValid)
            {
                foreach (string userId in model.AddUsers ?? new string[] { })
                {
                    IdentityUser user = await _userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        results = await _userManager.AddToRoleAsync(user, model.Role);
                        if (!results.Succeeded)
                        {
                            foreach (IdentityError error in results.Errors)
                            {
                                ModelState.AddModelError("", error.Description);
                            }
                        }
                    }
                }

                foreach (string userId in model.RemoveUsers ?? new string[] { })
                {
                    IdentityUser user = await _userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        results = await _userManager.RemoveFromRoleAsync(user, model.Role);
                        if (!results.Succeeded)
                        {
                            foreach (IdentityError error in results.Errors)
                            {
                                ModelState.AddModelError("", error.Description);
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            return View("ModifierRoleUsager", new RoleMembresViewModel { RoleId = model.RoleId });
        }


    }
}
