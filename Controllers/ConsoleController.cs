﻿using CoolGames.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CoolGames.Controllers
{
    public class ConsoleController : Controller
    {
        private readonly IConsoleRepository _consoleRepository;

        public ConsoleController(IConsoleRepository consoleRepository)
        {
            _consoleRepository = consoleRepository;
        }
        /// <summary>
        /// Permet d'afficher la liste de la console et les jeux associé
        /// </summary>
        /// <returns></returns>
        public ViewResult ListePs5()
        {
            return View(_consoleRepository.ListeConsolesPs5);
        }
        /// <summary>
        /// Permet d'afficher la liste de la console et les jeux associé
        /// </summary>
        /// <returns></returns>
        public ViewResult ListeXbox()
        {
            return View(_consoleRepository.ListeConsolesXbox);
        }
        /// <summary>
        /// Permet d'afficher la liste de la console et les jeux associé
        /// </summary>
        /// <returns></returns>
        public ViewResult ListeSwitch()
        {
            return View(_consoleRepository.ListeConsolesSwitch);
        }
        /// <summary>
        /// Permet d'afficher la page d'une console à modifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ViewResult Modifier(int id)
        {
            ConsoleJeu console = _consoleRepository.GetConsole(id);
            return View(console);
        }
        /// <summary>
        /// Permet de modifier la console dépendament du nom de la console
        /// </summary>
        /// <param name="console"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Modifier(ConsoleJeu console)
        {
            if (ModelState.IsValid)
            {
                if (console.Nom == "PlayStation 5")
                {
                    return RedirectToAction(nameof(ListePs5),new { id = console.Id });
                }
                else if (console.Nom == "Xbox Series X")
                {
                    return RedirectToAction(nameof(ListeXbox), new { id = console.Id });
                }
                else if (console.Nom == "Nintendo Switch")
                {
                    return RedirectToAction(nameof(ListeSwitch), new { id = console.Id });
                }
            }

            return View();
        }
    }
}
