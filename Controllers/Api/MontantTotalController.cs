﻿using CoolGames.Models;
using CoolGames.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace CoolGames.Controllers.Api
{
    [Route("api/Jeu")]
    [ApiController]
    public class MontantTotalController : ControllerBase
    {
        
        private readonly IJeuRepository _jeuRepository;

        public MontantTotalController(IJeuRepository jeuRepository)
        {
            
            _jeuRepository = jeuRepository;
        }
        [HttpGet]
        public IActionResult GetPrixTotaltJeu()
        {
            var mesJeux = _jeuRepository.ListeJeux.Where(j => j.Quantite < 75).ToList();
            

            string contenuFichier = System.IO.File.ReadAllText(@"wwwroot/SchemasJson/jeux-json-schema.json");
            JSchema schemaJsonJeux = JSchema.Parse(contenuFichier);

            JArray gateauxJson = JArray.FromObject(mesJeux);

            bool gateauxJsonValide = gateauxJson.IsValid(schemaJsonJeux, out IList<string> erreurs);

            if (!gateauxJsonValide)
            {
                return BadRequest(string.Join(", ", erreurs));
            }
            return Ok(mesJeux);

        }
    }
}
