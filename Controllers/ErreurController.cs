﻿using CoolGames.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CoolGames.Controllers
{
    public class ErreurController : Controller
    {
        /// <summary>
        /// Méthode pour gérer l'erreur 404
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [Route("Erreurs/Statut{code}")]
        public ViewResult GererErreur(int code)
        {
            ErreurViewModel erreurViewModel = new ErreurViewModel
            {
                CodeErreur = code,
            };

            if(code == 404)
            {
                erreurViewModel.Message = "Nous sommes vraiment désolée, mais cette page n'existe pas :(";
            }

            return View("Erreur",erreurViewModel);
        }
        /// <summary>
        /// Méthode pour gérer les autre Erreur(500 par exemple)
        /// </summary>
        /// <returns></returns>
        [Route("/ErreurGlobale")]
        public ViewResult ErreurGlobale()
        {
            ErreurViewModel erreurViewModel = new ErreurViewModel 
            {
                CodeErreur = HttpContext.Response.StatusCode,
                Message = "Oopsie, Une erreur s'est produite durant l'exécution de votre requête!"
            };

            return View("Erreur", erreurViewModel);
        }
    }
}
