﻿namespace CoolGames.ViewModels
{
    public class ErreurViewModel
    {
        public int CodeErreur { get; set; }
        public string Message { get; set; }
    }
}
