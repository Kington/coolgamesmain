﻿using CoolGames.Models;

namespace CoolGames.ViewModels
{
    public class PanierViewModel
    {
        public List<Panier> ListeJeuxPanier { get; set; }
        public decimal MontantTotal { get; set; }

    }
}
