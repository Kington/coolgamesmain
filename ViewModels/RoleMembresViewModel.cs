﻿using Microsoft.AspNetCore.Identity;

namespace CoolGames.ViewModels
{
    public class RoleMembresViewModel
    {
        public string Role { get; set; }
        public string RoleId { get; set; }
        public IList<IdentityUser> Membres { get; set; }
        public List<IdentityUser> NonMembres { get; set; }

        public string[] AddUsers { get; set; }
        public string[] RemoveUsers { get; set; }
    }
}
